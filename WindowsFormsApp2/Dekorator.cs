﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp2
{
    public class Dekorator : UzytkownikDrogi
    {

        protected UzytkownikDrogi userD;

        public Dekorator(UzytkownikDrogi user)
        {
            userD = user;
        }

        public override int GetSpeed()
        {
            return userD.GetSpeed();
        }


        public override Image GetImageIcon()
        {
            return userD.GetImageIcon();
        }

    }
}
