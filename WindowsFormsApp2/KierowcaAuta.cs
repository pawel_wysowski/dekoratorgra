﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using WindowsFormsApp2.Properties;

namespace WindowsFormsApp2
{
    public class KierowcaAuta : UzytkownikDrogi
    {

        public override int GetSpeed()
        {
            return 3;
        }

        public override Image GetImageIcon()
        {
            return Resources.car;
        }

    }
}
