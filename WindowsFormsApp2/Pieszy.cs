﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using WindowsFormsApp2.Properties;

namespace WindowsFormsApp2
{
   public class Pieszy : UzytkownikDrogi
    {

        public override int GetSpeed()
        {
            return 0;
        }

        public override Image GetImageIcon()
        {
            return Resources.ped;
        }

    }
}
