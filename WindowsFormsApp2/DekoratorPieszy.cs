﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp2
{
    public class DekoratorPieszy : Dekorator
    {

        public DekoratorPieszy(UzytkownikDrogi user) : base(user) { }

        public override int GetSpeed()
        {
            return base.GetSpeed()+500;
        }

        public override Image GetImageIcon()
        {
            return base.GetImageIcon();
        }
    }
}
