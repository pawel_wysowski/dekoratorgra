﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace WindowsFormsApp2
{
    public partial class Form1 : Form
    {
        List<PictureBox> users = new List<PictureBox>();

        UzytkownikDrogi u1 = new Pieszy();
        UzytkownikDrogi u2 = new Pieszy();

        UzytkownikDrogi u3 = new KierowcaAuta();
        UzytkownikDrogi u4 = new KierowcaAuta();

        UzytkownikDrogi u5 = new Rowerzysta();
        UzytkownikDrogi u6 = new Rowerzysta();

        XDirection currentXDirection;
        int x = 0;
        int num = 0;
        int moveDistanceX = 50;
        int moveDistanceY = 50;

        enum XDirection
        {
            left,
            right,
            up,
            down
        }


        public Form1()
        {
            InitializeComponent();
            
            u1 = new DekoratorPieszy(u1);
            u2 = new DekoratorPieszy(u2);
            u3 = new DekoratorAuto(u3);
            u4 = new DekoratorAuto(u4);
            u5 = new DekoratorRower(u5);
            u5 = new DekoratorRower(u6);
            AddUserView(u1);
            AddUserView(u2);
            AddUserView(u3);
            AddUserView(u4);
            AddUserView(u5);
            AddUserView(u6);

            timer1.Interval = u1.GetSpeed();
            timer2.Interval = u2.GetSpeed();
            timer3.Interval = u3.GetSpeed();
            timer4.Interval = u4.GetSpeed();
            timer5.Interval = u5.GetSpeed();
            timer6.Interval = u6.GetSpeed();


            timer1.Start();
            System.Threading.Thread.Sleep(60);
            timer2.Start();
            System.Threading.Thread.Sleep(60);
            timer3.Start();
            System.Threading.Thread.Sleep(60);
            timer4.Start();
            System.Threading.Thread.Sleep(60);
            timer5.Start();
            System.Threading.Thread.Sleep(60);
        }


        void moveUser(PictureBox p)
        {
            RandomDirection();
            switch (currentXDirection)
            {
                case XDirection.right:
                    Point point = new Point(p.Location.X + moveDistanceX, p.Location.Y);
                    if (point.X >= 800)
                       point = p.Location;
                    p.Location = point;
                    CheckPositions();
                    break;
                case XDirection.left:
                    Point point1 = new Point(p.Location.X - moveDistanceX, p.Location.Y);
                    if (point1.X <=-50)
                        point1 = p.Location;
                    p.Location = point1;
                    CheckPositions();
                    break;
                case XDirection.down:
                    Point point2 = new Point(p.Location.X, p.Location.Y + moveDistanceY);
                    if (point2.Y >=600)
                     point2 = p.Location;
                    p.Location = point2;
                    break;
                case XDirection.up:
                    Point point3 = new Point(p.Location.X, p.Location.Y - moveDistanceY);
                    if (point3.Y <= -50)
                        point3 = p.Location;
                    p.Location = point3;
                    break;

                    

            }
        }

        void CheckPositions()
        {

            if (users[0].Location == users[1].Location 
                || users[0].Location == users[2].Location 
                || users[0].Location == users[3].Location
                || users[0].Location == users[4].Location
                || users[0].Location == users[5].Location
                || users[1].Location == users[2].Location 
                || users[1].Location == users[3].Location 
                || users[1].Location == users[4].Location
                || users[1].Location == users[5].Location
                || users[2].Location == users[3].Location
                || users[2].Location == users[4].Location
                || users[2].Location == users[5].Location
                || users[3].Location == users[4].Location
                || users[3].Location == users[5].Location
                || users[4].Location == users[5].Location
                )
            {
                timer1.Stop();
                timer2.Stop();
                timer3.Stop();
                timer4.Stop();
                timer5.Stop();
                timer6.Stop();
                MessageBox.Show("BOOOM");
            }
        }

        void RandomDirection()
        {
            Random r = new Random();
            int choice = r.Next(0,4);
            currentXDirection = (XDirection)choice;
        }

        


        void ChangeXDirection()
        {

            switch (currentXDirection)
                {
                case XDirection.right:
                    moveDistanceX = -50;
                    currentXDirection = XDirection.left;
                    break;
                case XDirection.left:
                    moveDistanceX = 50;
                    currentXDirection = XDirection.right;
                    break;
                case XDirection.up:
                    moveDistanceY = -50;
                    currentXDirection = XDirection.down;
                    break;
                }
        }

        void AddUserView(UzytkownikDrogi u)
        {
            PictureBox picture = new PictureBox();
            picture.Name = "pictureBox"+num;
            picture.Size = new Size(50, 50);
            picture.Location = new Point(50+users.Count*100, 250);

            picture.Image = u.GetImageIcon();
            picture.Visible = true;
            users.Add(picture);
            Controls.Add(picture);
            num++;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            moveUser(users[0]);

        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            moveUser(users[1]);
        }

        private void timer3_Tick(object sender, EventArgs e)
        {
            moveUser(users[2]);
        }

        private void timer4_Tick(object sender, EventArgs e)
        {
            moveUser(users[3]);
        }

        private void timer5_Tick(object sender, EventArgs e)
        {
            moveUser(users[4]);
        }

        private void timer6_Tick(object sender, EventArgs e)
        {
            moveUser(users[5]);
        }
    }
}
