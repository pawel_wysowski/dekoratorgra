﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace WindowsFormsApp2
{
    public abstract class UzytkownikDrogi
    {
        public abstract int GetSpeed();
        public abstract Image GetImageIcon();
    }
}
