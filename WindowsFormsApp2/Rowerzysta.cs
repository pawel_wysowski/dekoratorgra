﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using WindowsFormsApp2.Properties;

namespace WindowsFormsApp2
{
   public class Rowerzysta : UzytkownikDrogi
    {

        public override int GetSpeed()
        {
            return 2;
        }

        public override Image GetImageIcon()
        {
            return Resources.bike;
        }

    }
}
